package world;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RecursiveUnzip {

	public HashMap<String, HashMap<String, Integer>> ocurrences;
	public ArrayList<String> total;

	public static final Pattern DISENO_FOLDER = Pattern
			.compile("home/lidie/([A-Za-z0-9_\\-\\.]+/){3}(diseno_grafico)$");
	
	public static final Pattern PARENT_DISENO_FOLDER = Pattern
			.compile("home/lidie/([A-Za-z0-9_\\-\\.]+/){3}(diseno_grafico)/");

	public static final Pattern OBJETOS_FOLDER = Pattern
			.compile("home/lidie/([A-Za-z0-9_\\-\\.]+/){3}(objetos_educativos)$");
	
	public static final Pattern PARENT_OBJETOS_FOLDER = Pattern
			.compile("home/lidie/([A-Za-z0-9_\\-\\.]+/){3}(objetos_educativos)/");

	public static final Pattern COURSE_FOLDER = Pattern
			.compile("home/lidie/([A-Za-z0-9_\\-\\.]+/){2}([A-Za-z0-9_\\-\\.]+)$");

	public static final Pattern PARENT_COURSE_FOLDER = Pattern
			.compile("home/lidie/([A-Za-z0-9_\\-\\.]+/){2}[A-Za-z0-9_\\-\\.]+/");
	
	public static final Pattern MYWEBCT_FOLDER = Pattern
			.compile("home/lidie/([A-Za-z0-9_\\-\\.]+/){3}[A-Za-z0-9_\\-\\.]+/(mywebct)$");

	public static final Pattern PARENT_MYWEBCT_FOLDER = Pattern
			.compile("home/lidie/([A-Za-z0-9_\\-\\.]+/){3}[A-Za-z0-9_\\-\\.]+/(mywebct)/");

	public static final Pattern ZIP_FILE = Pattern
			.compile("[bB]lackboard/([A-Za-z0-9_\\-\\.]+)\\.zip");
	
	public static final Pattern BACKUP_FILE = Pattern
			.compile("/[bB]lackboard/");
	
	public static final Pattern IMAGE_FILE = Pattern
			.compile("/([A-Za-z0-9_\\-\\.]+)\\.(gif|jpeg|jpg|png)$");

	public static void main(String[] aArgs) {

		RecursiveUnzip fw = new RecursiveUnzip();

		try {
			fw.walk("/home/lidie/");

			System.out.println("CORRECT FOLDERS:" + fw.ocurrences.toString());
			String findIncorrect = "";
			ArrayList<String> incorrect = new ArrayList<String>();
			for (int i = 0; i < fw.total.size(); i++) {
				if (fw.ocurrences.get(fw.total.get(i)) == null || fw.ocurrences.get(fw.total.get(i)).size() < 3){
					incorrect.add(fw.total.get(i));
					findIncorrect += "find /home/lidie/ -maxdepth 3 -name '"+fw.total.get(i).replace("/", "")+"' -type d;";
				}
			}

			System.out.println("INCORRECT FOLDERS:" + incorrect.toString());
			System.out.println("INCORRECT:" + incorrect.size());
			
			String[] commands = { findIncorrect };
			executeCommands(commands);
			
			System.out.println("CORRECT FOLDERS:" + fw.ocurrences.size());
			System.out.println("TOTAL COURSES:" + fw.total.size());

		} catch (Exception e1) {
			System.err.println(e1);
			System.exit(1);
		}

	}

	public RecursiveUnzip() {
		ocurrences = new HashMap<String, HashMap<String, Integer>>();
		total = new ArrayList<String>();
	}

	public void walk(String path) throws Exception {

		File root = new File(path);
		File[] list = root.listFiles();

		for (File f : list) {
			if (f.isDirectory()) {

				String mainFile = f.getAbsoluteFile().getAbsolutePath();

				Matcher matchDiseno = DISENO_FOLDER.matcher(mainFile);
				Matcher matchObjetos = OBJETOS_FOLDER.matcher(mainFile);
				Matcher matchMyWeb = MYWEBCT_FOLDER.matcher(mainFile);
				Matcher matchTotal = COURSE_FOLDER.matcher(mainFile);

				if (matchTotal.find())
					total.add(matchTotal.group(2)+"/");

				if (matchObjetos.find()) {
					buildCourseHash(f, matchObjetos);
				}
				else if (matchDiseno.find()) {
					buildCourseHash(f, matchDiseno);
				}
				else if (matchMyWeb.find()) {
					buildCourseHash(f, matchMyWeb);
				}
				walk(f.getAbsolutePath());
				// System.out.println( "CORRECT FOLDERS FOUND:" +
				// ocurrences.size() );
			} else if (f.isFile()) {
				String mainFile = f.getAbsoluteFile().getAbsolutePath();
				Matcher matchDiseno = PARENT_DISENO_FOLDER.matcher(mainFile);
				Matcher matchObjetos = PARENT_OBJETOS_FOLDER.matcher(mainFile);
				Matcher matchMyWeb = PARENT_MYWEBCT_FOLDER.matcher(mainFile);

				if (matchObjetos.find()) {
					verifyStructureParent(f, matchObjetos);
				}
				else if (matchDiseno.find()) {
					verifyStructureParent(f, matchDiseno);
				}
				else if (matchMyWeb.find()) {
					verifyStructureParent(f, matchMyWeb);
				}	
				// unzipFile(f);

			}
		}
	}
	
	public void verifyStructureParent(File f, Matcher matched){
		HashMap<String, Integer> courseHash = ocurrences.get(matched.group(1));
		courseHash.put(matched.group(2), courseHash.get(matched.group(2))+1);
		ocurrences.put(matched.group(1), courseHash);
	}
	
	public void buildCourseHash(File f, Matcher matched) throws Exception{
		//String parentFolder = f.getParentFile() + "";
		HashMap<String, Integer> courseHash = ocurrences.get(matched.group(1));
		if (courseHash == null) {
			courseHash = new HashMap<String, Integer>();
			courseHash.put(matched.group(2), new Integer(0));
		}
		else{
			courseHash.put(matched.group(2), new Integer(0));
		}
		ocurrences.put(matched.group(1), courseHash);
		
		String files = "du -sh " + f.getAbsolutePath();
		String[] commands = { files };
		executeCommands(commands);
	}

	public void unzipFile(File f) throws Exception {
		String mainFile = f.getAbsoluteFile().getAbsolutePath();

		Matcher matchDTI = ZIP_FILE.matcher(mainFile);
		if (matchDTI.find()) {
			String template = "/home/lidie/Migraciones_Temporal/template/ ";

			String createTemplateCommand = "mkdir " + f.getParentFile() + "/"
					+ matchDTI.group(1);
			String copyToTemplateCommand = "cp -R " + template
					+ f.getParentFile() + "/" + matchDTI.group(1);
			String unzipCommand = "unzip -u " + f.getAbsoluteFile() + " -d "
					+ f.getParentFile() + "/" + matchDTI.group(1);
			String chOwn = "chown -R www-data " + f.getParentFile();

			String[] commands = { createTemplateCommand, copyToTemplateCommand,
					unzipCommand, chOwn };
			executeCommands(commands);
		}
	}

	public static void executeCommands(String[] commands) throws Exception {
		for (String command : commands) {
			Process proc = Runtime.getRuntime().exec(command);

			StreamGobbler errorGobbler = new StreamGobbler(
					proc.getErrorStream(), "ERROR");

			// any output?
			StreamGobbler outputGobbler = new StreamGobbler(
					proc.getInputStream(), "OUTPUT");

			// kick them off
			errorGobbler.start();
			outputGobbler.start();

			// any error???
			int exitVal = proc.waitFor();
			System.out.println("ExitValue: " + exitVal + "\nCommands: "
					+ command);

		}
	}

}
