package world;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Auto {

	public final static Pattern F_LLAVE_PLACE = Pattern
			.compile(
					"(#:\\s(.+\\.(ctp)|(php)):([0-9]+;?)+\\s*)+\\s*msgid \"(.+)\"\\s*msgstr \"(.+)\"",
					Pattern.MULTILINE);

	public final static String FOLDER = "/home/eric/git/Ekarda/";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Pattern idPattern = Pattern.compile("msgid \"(.+)\"",
					Pattern.MULTILINE);
			
			Pattern strPattern = Pattern.compile("msgstr \"(.+)\"",
					Pattern.MULTILINE);
			
			Matcher codigo;

			FileInputStream fstream = new FileInputStream(FOLDER
					+ "config/FIXES.txt");
			// Get the object of DataInputStream
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String readData;
			String[] files;
			ArrayList<String[]> filesTotal = new ArrayList<String[]>();
			String issue = "";
			String replacement = "";
			// Read File Line By Line
			while ((readData = br.readLine()) != null) {
				// Print the content on the console
				if (readData.startsWith("#:")) {
					files = readData.split(" ");
					filesTotal.add(files);
				} else if (readData.startsWith("msgid")) {
					codigo = idPattern.matcher(readData);
					if (codigo.find())
						issue = codigo.group(1);
				} else if (readData.startsWith("msgstr")) {
					codigo = strPattern.matcher(readData);
					if (codigo.find())
						replacement = codigo.group(1);
				} else if (readData.equals("")
						&& !replacement.matches("msgstr \"\"") && replacement.length()>0) {
					// System.out.println(filesTotal);
					cleanFiles(issue, replacement, filesTotal);
					replacement = "";
					issue = "";
					filesTotal = new ArrayList<String[]>();

				}
			}

			// String fixes = readFileAsString(FOLDER+"config/FIXES.txt");
			// String constants =
			// readFileAsString(FOLDER+"config/constant.php");

			// Matcher codigo = F_LLAVE_PLACE.matcher(fixes);
			// while (codigo.find()) {
			// Pattern linePattern =
			// Pattern.compile(codigo.group(3),Pattern.MULTILINE);

			// Matcher codigoLine = linePattern.matcher(constants);
			// System.out.println(codigo.group(1));
			// if(codigoLine.find()){
			// System.out.println("\""+codigo.group(3)+"\"");
			// }
			// constants = codigoLine.replaceAll(codigo.group(4));
			/*
			 * System.out.println(codigo.group(1) + codigo.group(2) +
			 * codigo.group(3) + codigo.group(4));
			 */
			// }

			// BufferedWriter out = new BufferedWriter(new
			// FileWriter(FOLDER+"config/constant.php"));
			// out.write(constants);
			// out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void cleanFiles(String issue, String replacement,
			ArrayList<String[]> files) {
		for (int i = 0; i < files.size(); i++) {
			for (int j = 1; j < files.get(i).length; j++) {
				try {
					String file = files.get(i)[j].split(":")[0];
					String fixes = readFileAsString(FOLDER + file);
					String compare = fixes;
					issue = issue.replace(".", "\\.");
					issue = issue.replace(")", "\\)");
					issue = issue.replace("(", "\\(");
					Pattern linePattern = Pattern.compile("\""+issue+"\"",
							Pattern.MULTILINE);
					Matcher codigoLine = linePattern.matcher(fixes);
					fixes = codigoLine.replaceAll("\""+replacement+"\"");
					//if(file.contains("constant.php")){
						System.out.println(file);
						BufferedWriter out = new BufferedWriter(new FileWriter(
								FOLDER + file));
						out.write(fixes);
						out.close();
					//}
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println(issue);
					System.out.println(replacement+"/////");
					continue;
				}
			}

		}
		// String fixes = readFileAsString(FOLDER + "config/FIXES.txt");
		// String constants = readFileAsString(FOLDER + "config/constant.php");
		//
		// Matcher codigo = F_LLAVE_PLACE.matcher(fixes);
		// while (codigo.find()) {
		// Pattern linePattern = Pattern.compile(codigo.group(3),
		// Pattern.MULTILINE);
		//
		// Matcher codigoLine = linePattern.matcher(constants);
		// System.out.println(codigo.group(1));
		// if (codigoLine.find()) {
		// System.out.println("\"" + codigo.group(3) + "\"");
		// }
		// constants = codigoLine.replaceAll(codigo.group(4));
		// /*
		// * System.out.println(codigo.group(1) + codigo.group(2) +
		// * codigo.group(3) + codigo.group(4));
		// */
		// }
		//
		// BufferedWriter out = new BufferedWriter(new FileWriter(FOLDER
		// + "config/constant.php"));
		// out.write(constants);
		// out.close();
	}

	/**
	 * @param filePath
	 *            the name of the file to open. Not sure if it can accept URLs
	 *            or just filenames. Path handling could be better, and buffer
	 *            sizes are hardcoded
	 */
	public static String readFileAsString(String filePath)
			throws java.io.IOException {
		StringBuffer fileData = new StringBuffer(1000);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		char[] buf = new char[1024];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
			buf = new char[1024];
		}
		reader.close();
		return fileData.toString();
	}

	public static void replaceLine(String file, String lineNumber, String goal,
			String replacement) {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(new File("input.txt"))));

			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(
					"output.txt")));
			// for performance compile once and use repeatedly (instead of
			// line.matches("four.*"))
			Pattern pattern = Pattern.compile(goal); // brackets only
														// needed if using
														// regex replace
														// below ...
			String line;
			while ((line = reader.readLine()) != null) {
				Matcher matcher = pattern.matcher(line);
				if (matcher.matches()) {
					line = line + "6,"; // your data
					// line = matcher.replaceAll("$16,"); // or with
					// regex-replace ... "$1" is the first "()" in the pattern
					// above
				}
				writer.append(line);
				writer.newLine();
				// writer.flush(); // to see everything that is written
				// immediately in the file [it's buffered otherwise]
			}
			reader.close();
		} catch (Exception e) {

		}
	}

}
