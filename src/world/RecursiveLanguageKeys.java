package world;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RecursiveLanguageKeys {

	public String finalFile;
	public HashMap<String, String> ocurrences;

	public static final Pattern MESSAGE_STRING = Pattern
			.compile("__n?\\('([^']+)',",
					Pattern.MULTILINE);
	public static final Pattern MESSAGE_QUOTE_STRING = Pattern
			.compile("__n?\\(\"([^\"]+)\",",
					Pattern.MULTILINE);
	
	public static final Pattern TARGET_FILE = Pattern
			.compile(
					".+\\.(ctp)|(php)");
	
	public static final Pattern idPattern = Pattern.compile("msgid \"(.+)\"",
			Pattern.MULTILINE);
	
	private String languageKeysFile;

	public static void main(String[] aArgs) {

		RecursiveLanguageKeys fw = new RecursiveLanguageKeys();

		try {
			fw.walk("/var/www/cn.ekarda/");

			Set<String> messages = fw.ocurrences.keySet();
			for (String message : messages) {
				String poFileFormat = fw.ocurrences.get(message)+"\nmsgid \""+message+"\"\nmsgstr \"\"\n";
				fw.finalFile += poFileFormat;
			}
			System.out.println(fw.finalFile);
			BufferedWriter out = new BufferedWriter(new FileWriter("LK.txt"));
			out.write(fw.finalFile);
			out.close();
		} catch (Exception e1) {
			System.err.println(e1);
			System.exit(1);
		}

	}

	public RecursiveLanguageKeys() {
		String fileString = "/var/www/cn.ekarda/locale/eng/LC_MESSAGES/default.po";
		finalFile = "";
		ocurrences = new HashMap<String, String>();
		try {
			languageKeysFile = Auto.readFileAsString(fileString);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void walk(String path) throws Exception {

		File root = new File(path);
		File[] list = root.listFiles();

		for (File f : list) {
			if (f.isDirectory()) {

				
				walk(f.getAbsolutePath());
			} else if (f.isFile()) {
				String mainFile = f.getAbsoluteFile().getAbsolutePath();
				Matcher matchDiseno = TARGET_FILE.matcher(mainFile);
				if (matchDiseno.find()){
					String fileString = Auto.readFileAsString(mainFile);
					mainFile = mainFile.replace("/var/www/cn.ekarda", "");
					Matcher matchMessage = MESSAGE_STRING.matcher(fileString);
					Matcher matchMessageQuote = MESSAGE_QUOTE_STRING.matcher(fileString);
					while (matchMessage.find()){
						findMessage(matchMessage.group(1), mainFile);
					}
					while (matchMessageQuote.find()){
						findMessage(matchMessageQuote.group(1), mainFile);
					}
				}

			}
		}
	}
	
	public void findMessage(String message, String mainFile){
		String originalMessage = message;
		message = message.replace(".", "\\.");
		message = message.replace(")", "\\)");
		message = message.replace("(", "\\(");
		message = message.replace("{", "\\{");
		message = message.replace("}", "\\}");
		Pattern idPattern = Pattern.compile("msgid \""+message+"\"",
				Pattern.MULTILINE);
		Matcher matchMessage = idPattern.matcher(languageKeysFile);
		if (!matchMessage.find()){
			String newOcurrence = ocurrences.get(originalMessage) != null? ocurrences.get(originalMessage): "";
			ocurrences.put(originalMessage, newOcurrence+"\n#: "+mainFile);
//			String poFileFormat = "\n#: "+mainFile+"\nmsgid \""+originalMessage+"\"\nmsgstr \"\"\n";
//			finalFile += poFileFormat;
		}
	}



}
